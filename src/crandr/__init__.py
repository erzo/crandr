"""
This is a unifying wrapper around xrandr and swaymsg to control your monitors/outputs,
mainly intended for setups where only one or two monitors are enabled at the same time.
"""

from .version import __version__

__all__ = ['__version__']
